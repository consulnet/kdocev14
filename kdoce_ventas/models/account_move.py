# -*- coding: utf-8 -*-

#./odoo-bin --dev all -c odoo.conf -u kdoce_ventas

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo import _
import logging
_logger = logging.getLogger(__name__)
#_logger.debug('Mensaje de debug')

class AccountMoveExtendido(models.Model):
    _inherit = 'account.move'

    terreno_id = fields.Many2one(   string="Asesor de Terreno",
                                    comodel_name="res.users")

    factura_devuelta = fields.Boolean(string="Factura devuelta")

    colegio_rbd = fields.Char(string="RBD", related="partner_id.x_studio_rbd", readonly=True)

    monto_exento = fields.Monetary(string='Monto exento', store=True, readonly=True, tracking=True, compute='_compute_amount_exento')

    monto_afecto = fields.Monetary(string='Monto afecto', store=True, readonly=True, tracking=True, compute='_compute_amount_afecto')

    invoice_partner_display_name = fields.Char(compute='_compute_invoice_partner_display_info_colegios', store=True)

    @api.depends('partner_id', 'invoice_source_email', 'partner_id.name')
    def _compute_invoice_partner_display_info_colegios(self):
        for move in self:
            vendor_display_name = move.partner_id.display_name
            if move.partner_id.x_studio_es_colegio:
                vendor_display_name = move.partner_id.x_studio_nombre_sostenedor
                move.colegio_rbd = move.partner_id.x_studio_rbd
            if not vendor_display_name:
                if move.invoice_source_email:
                    vendor_display_name = _('@From: %(email)s', email=move.invoice_source_email)
                else:
                    vendor_display_name = '#Created by: USER'
            move.invoice_partner_display_name = vendor_display_name


    @api.depends(
        'line_ids.matched_debit_ids.debit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.matched_credit_ids.credit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        'line_ids.payment_id.state',
        'line_ids.full_reconcile_id')
    def _compute_amount_exento(self):
        for move in self:
            total_monto_exento = 0.0
            for line in move.line_ids:
                if len(line.tax_ids):
                    for tax in line.tax_ids:
                        if tax.amount == 0.0:
                            total_monto_exento += line.quantity * line.price_unit * (1.0 - (line.discount / 100.0 ) )
                elif not line.exclude_from_invoice_tab:
                    total_monto_exento += line.quantity * line.price_unit * (1.0 - (line.discount / 100.0 ) )

            move.monto_exento = total_monto_exento

    @api.depends(
        'line_ids.matched_debit_ids.debit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_debit_ids.debit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.matched_credit_ids.credit_move_id.move_id.payment_id.is_matched',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual',
        'line_ids.matched_credit_ids.credit_move_id.move_id.line_ids.amount_residual_currency',
        'line_ids.debit',
        'line_ids.credit',
        'line_ids.currency_id',
        'line_ids.amount_currency',
        'line_ids.amount_residual',
        'line_ids.amount_residual_currency',
        'line_ids.payment_id.state',
        'line_ids.full_reconcile_id')
    def _compute_amount_afecto(self):
        for move in self:
            total_monto_afecto = 0.0
            for line in move.line_ids:
                if len(line.tax_ids):
                    for tax in line.tax_ids:
                        if tax.amount != 0.0:
                            total_monto_afecto += line.quantity * line.price_unit * (1.0 - (line.discount / 100.0 ) )
            move.monto_afecto = total_monto_afecto

