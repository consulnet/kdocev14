# -*- coding: utf-8 -*-

#./odoo-bin --dev all -c odoo.conf -u kdoce_ventas

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo import _
import logging
_logger = logging.getLogger(__name__)
#_logger.debug('Mensaje de debug')

class PurchaseOrderExtendido(models.Model):
    _inherit = 'purchase.order'

    country_id = fields.Many2one(string="País", 
                                related="partner_id.country_id", 
                                readonly=True,
                                store=True)


