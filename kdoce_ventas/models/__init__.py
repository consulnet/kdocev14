# -*- coding: utf-8 -*-

from . import calendar_event
from . import account_move
from . import account_payment
from . import stock_picking
from . import crm_lead
from . import purchase_order
