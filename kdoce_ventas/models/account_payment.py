# -*- coding: utf-8 -*-

#./odoo-bin --dev all -c odoo.conf -u kdoce_ventas

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo import _

class AccountMoveExtendido(models.Model):
    _inherit = 'account.payment'

    factura_recibida = fields.Boolean(string="Factura recibida")

