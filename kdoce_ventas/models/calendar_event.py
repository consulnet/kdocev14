# -*- coding: utf-8 -*-

#./odoo-bin --dev all -c odoo.conf -u kdoce_ventas

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class CalendarEventExtendido(models.Model):
    _inherit = 'calendar.event'

    colegio_id = fields.Many2one(   string="Colegio",
                                    comodel_name="res.partner",
                                    readonly=True)

    asesor_id = fields.Many2one(    string="Asesor",
                                    comodel_name="res.users",
                                    default=lambda self: self.env.user and self.env.user.id or False)


    colegio_region = fields.Many2one(string="Región",
                                    comodel_name="res.country.state",
                                    related="colegio_id.state_id",
                                    readonly=True)

    colegio_comuna = fields.Char(string="Comuna",
                                    related="colegio_id.city",
                                    readonly=True)

    colegio_rbd = fields.Char(related="colegio_id.x_studio_rbd", readonly=True)

    colegio_matricula = fields.Integer(related="colegio_id.x_studio_matricula", readonly=True)
    
    tipo_presentacion = fields.Selection(string="Tipo de Presentación",
                                 selection=[
                                            ('virtual', 'Virtual'),
                                            ('presentacion', 'Presentación'),
                                            ('reunion', 'Reunión'),
                                            ('cierre', 'Cierre de Venta'),
                                            ('tecnica', 'Técnica')
                                            ],
                                default="reunion")
                                
    validacion_asesor = fields.Selection(string="Validación asesor",
                                 selection=[
                                            ('realizada', 'REALIZADA'),
                                            ('no realizada', 'NO REALIZADA'),
                                            ('suspendida', 'SUSPENDIDA')
                                            ],
                                default="no realizada")
    
    validacion_telefonico = fields.Selection(string="Validación teléfonico",
                                 selection=[
                                            ('realizada', 'REALIZADA'),
                                            ('no realizada', 'NO REALIZADA'),
                                            ('suspendida', 'SUSPENDIDA')
                                            ],
                                default="no realizada")

    @api.onchange('partner_ids')
    def _search_school(self):
        for record in self:
            for partner in record.partner_ids:
                if partner.x_studio_es_colegio:
                    record.colegio_id = partner.id

    @api.constrains('partner_ids')
    def _save_school_relation(self):
        for record in self:
            for partner in record.partner_ids:
                if partner.x_studio_es_colegio:
                    record.colegio_id = partner.id
