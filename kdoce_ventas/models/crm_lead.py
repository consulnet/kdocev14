# -*- coding: utf-8 -*-

#./odoo-bin --dev all -c odoo.conf -u kdoce_ventas

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo import _
import logging
_logger = logging.getLogger(__name__)
#_logger.debug('Mensaje de debug')

class CrmLeadExtendido(models.Model):
    _inherit = 'crm.lead'

    terreno_id = fields.Many2one(   string="Asesor de Terreno",
                                    comodel_name="res.users")

