# -*- coding: utf-8 -*-
{
    'name': 'KDOCE Ventas',
    'summary': "Modificaciones al sistema para el equipo de ventas",
    'description': """
        Los siguientes campos fueron agregados por Odoo Studio, no son parte de este módulo pero son necesarios para la gestión de ventas.
            res_parner
                x_studio_rbd
                x_studio_colegio_rural
                x_studio_latitud
                x_studio_longitud
                x_studio_convenio_pie
                x_studio_estado_establecimiento
                x_studio_orientacion_religiosa
                x_studio_matricula
                x_studio_estudiantes_prioritarios
                x_studio_estudiantes_preferenciales
                x_studio_estudiantes_preferentes
                x_studio_sep_mensual
                x_studio_sep_anual
                x_studio_segmento
                x_studio_nombre_sostenedor
                x_studio_dependencia
                x_studio_es_colegio
                x_studio_tipo_de_cliente
                x_studio_cargo
            crm_phonecall
                x_studio_tipo_de_llamada
        
        Funcionalidades en este módulo
            Extender la vista calendario y formulario de las reuniones para ver datos propios del colegio.
        

    """,
    'author': 'Matías Salomón',
    'website': 'https://kdoce.cl',
    'version': '0.1',
    'depends': ['calendar','account', 'l10n_cl', 'crm', 'purchase'],
    'data': [
        'views/calendar_event_views_inherit.xml',
        'views/account_move_views_inherit.xml',
        'views/purchase_order_views_inherit.xml',
        'views/account_payment_views_inherit.xml',
        'views/stock_picking_views_inherit.xml',
        'views/account_view_account_supplier_payment_tree.xml',
        'views/crm_lead_views_inherit.xml',
        'reports/account_payment_report_inherit.xml',
        'reports/account_move_informations_inherit_kdoce.xml'
    ],
    'demo': [

    ]
}
