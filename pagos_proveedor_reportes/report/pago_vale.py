# -*- coding: utf-8 -*-
# Part of inCore. See LICENSE file for full copyright and licensing details.

from odoo import api, models


class ReportPagoVale(models.AbstractModel):
    _name = 'account.payment.group'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report =  report_obj._get_report_from_name('account.payment.group')
        doc_args = {
            'doc_ids': docids,
            'doc_model': report.pagos_proveedor_reportes,
            #'doc_model': "agro_export",
            'docs': self,
        }
        return report_obj.render('account.payment.group', doc_args)
 
