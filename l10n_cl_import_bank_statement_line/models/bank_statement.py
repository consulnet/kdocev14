# -*- coding: utf-8 -*-
# Part of Konos. See LICENSE file for full copyright and licensing details.

import tempfile
import binascii
import logging
import re
from datetime import datetime
from odoo.exceptions import Warning
from odoo import models, fields, api, exceptions, _
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF

_logger = logging.getLogger(__name__)
try:
    import csv
except ImportError:
    _logger.debug('Cannot `import csv`.')
try:
    import xlwt
except ImportError:
    _logger.debug('Cannot `import xlwt`.')
try:
    import cStringIO
except ImportError:
    _logger.debug('Cannot `import cStringIO`.')
try:
    import base64
except ImportError:
    _logger.debug('Cannot `import base64`.')

try:
    import xlrd
except ImportError:
    _logger.debug('Cannot `import xlrd`.')


class account_bank_statement_wizard(models.TransientModel):
    _name = "account.bank.statement.wizard"

    file = fields.Binary('File')
    file_opt = fields.Selection(
        [('excel', 'Excel'), ('csv', 'CSV')], default='excel')
    bank_opt = fields.Selection([('santander', 'Santander'), (
        'estado', 'Banco Estado'), ('chile', 'Banco de Chile'), ('itau', 'Banco Itau'),
                                 ('scotiabank', 'Banco Scotiabank')])

    type_extract = fields.Selection(
        [('cartola historica', 'cartola historica'), ('cartola provisoria', 'cartola provisoria'),
         ('cartola movimientos', 'cartola movimientos'), ('cartola mensual', 'cartola mensual')])

    def import_file(self):
        # if not file:
        #    raise Warning('Please Select File')
        if self.file_opt == 'csv':
            keys = ['date', 'ref', 'partner', 'memo', 'amount']
            data = base64.b64decode(self.file)
            file_input = io.StringIO(data.decode("utf-8"))
            file_input.seek(0)
            reader_info = []
            reader = csv.reader(file_input, delimiter=',')

            try:
                reader_info.extend(reader)
            except Exception:
                raise exceptions.Warning(_("Not a valid file!"))
            values = {}
            for i in range(len(reader_info)):
                field = list(map(str, reader_info[i]))
                values = dict(zip(keys, field))
                if values:
                    if i == 0:
                        continue
                    else:
                        res = self._create_statement_lines(values)
        elif self.file_opt == 'excel':
            fp = tempfile.NamedTemporaryFile(suffix=".xlsx")
            fp.write(binascii.a2b_base64(self.file))
            fp.seek(0)
            values = {}
            workbook = xlrd.open_workbook(fp.name)
            sheet = workbook.sheet_by_index(0)
            contador = 0
            # TODO: Cartola semanal del banco santander
            if self.bank_opt == 'santander' and self.type_extract == 'cartola movimientos':
                nombre_cartola = sheet.cell_value(10, 5)
                saldo_fin = sheet.cell_value(12, 3)
                values = {'name': 'Cartola ' + nombre_cartola,
                          'balance_end_real': saldo_fin
                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(11, i) for i in range(sheet.ncols)]
                line = 12
                for row in range(12, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)

                    vals['statement_id'] = active_id
                    vals['amount'] = values_es['MONTO']
                    vals['date'] = datetime.strptime(
                        values_es['FECHA'], '%d/%m/%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['DESCRIPCIÓN MOVIMIENTO']
                    # vals['amount'] = values_es['MONTO']
                    vals['narration'] = values_es['CARGO/ABONO']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

            # TODO: Cartola Provisoria del banco santander durante el mes
            if self.bank_opt == 'scotiabank' and self.type_extract == 'cartola mensual':
                nombre_cartola = sheet.cell_value(1, 5)
                values = {'name': 'Hasta el' + nombre_cartola,

                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(10, i) for i in range(sheet.ncols)]

                line = 11
                for row in range(11, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)

                    vals['statement_id'] = active_id
                    vals['date'] = datetime.strptime(values_es['Fecha '], '%d-%m-%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['Descripción']
                    if values_es['Cargos']:
                        vals['amount'] = values_es['Cargos']
                    if values_es['Abonos']:
                        vals['amount'] = values_es['Abonos']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

            if self.bank_opt == 'scotiabank' and self.type_extract == 'cartola movimientos':
                nombre_cartola = sheet.cell_value(2, 2)
                saldo_fin = sheet.cell_value(10, 3)
                values = {'name': nombre_cartola,

                          'balance_end_real': saldo_fin
                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(18, i) for i in range(sheet.ncols)]

                line = 20
                for row in range(20, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)
                    import wdb;
                    wdb.set_trace()
                    vals['statement_id'] = active_id
                    vals['date'] = datetime.strptime(
                        values_es['Fecha'], '%d-%m-%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['Descripción']
                    if values_es['Cargos']:
                        vals['amount'] = values_es['Cargos']
                    if values_es['Abonos']:
                        vals['amount'] = values_es['Abonos']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

            if self.bank_opt == 'santander' and self.type_extract == 'cartola provisoria':
                nombre_cartola = sheet.cell_value(7, 0)
                saldo_ini = sheet.cell_value(10, 0)
                saldo_fin = sheet.cell_value(10, 3)
                values = {'name': nombre_cartola,
                          'balance_start': saldo_ini,
                          'balance_end_real': saldo_fin
                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(12, i) for i in range(sheet.ncols)]
                line = 13
                for row in range(13, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)
                    if values_es['MONTO'] == 'Saldos diarios':
                        break

                    vals['statement_id'] = active_id
                    vals['date'] = datetime.strptime(
                        values_es['FECHA'], '%d/%m/%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['DESCRIPCIÓN MOVIMIENTO']
                    vals['amount'] = values_es['MONTO']
                    vals['narration'] = values_es['CARGO/ABONO']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

            # TODO: Cartola historica de banco santander para el mes completo cerrado
            elif self.bank_opt == 'santander' and self.type_extract == 'cartola historica':
                nombre_cartola = sheet.cell_value(7, 0)
                saldo_ini = sheet.cell_value(10, 0)
                saldo_fin = sheet.cell_value(10, 6)
                values = {'name': nombre_cartola,
                          'balance_start': saldo_ini,
                          'balance_end_real': saldo_fin
                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(15, i) for i in range(sheet.ncols)]

                line = 16
                for row in range(16, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)

                    if values_es['MONTO'] == 'Resumen comisiones':
                        break
                    if values_es['MONTO'] == 'Saldos diarios':
                        break

                    vals['statement_id'] = active_id
                    vals['date'] = datetime.strptime(
                        values_es['FECHA'], '%d/%m/%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['DESCRIPCIÓN MOVIMIENTO']
                    vals['amount'] = values_es['MONTO']
                    vals['narration'] = values_es['CARGO/ABONO']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})


            elif self.bank_opt == 'chile':
                nombre_cartola = sheet.cell_value(13, 2)
                saldo_ini = sheet.cell_value(22, 0)
                saldo_fin = sheet.cell_value(22, 7)
                values = {'name': nombre_cartola,
                          'balance_start': saldo_ini,
                          'balance_end_real': saldo_fin
                          }
                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(21, i) for i in range(sheet.ncols)]

                line = 22
                for row in range(22, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)

                    vals['statement_id'] = active_id
                    vals['date'] = datetime.strptime(
                        values_es['Fecha'], '%d/%m/%Y').strftime('%Y-%m-%d')
                    vals['payment_ref'] = values_es['Descripción']
                    if values_es['Cargos (CLP)'] != '':
                        vals['amount'] = - + values_es['Cargos (CLP)']
                    if values_es['Cargos (CLP)'] == '':
                        vals['amount'] = values_es['Abonos (CLP)']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

            elif self.bank_opt == 'estado':
                for row_no in range(sheet.nrows):

                    if row_no <= 0:
                        fields = map(lambda row: row.value.encode(
                            'utf-8'), sheet.row(row_no))
                    else:
                        line = list(map(lambda row: isinstance(row.value, str) and row.value.encode(
                            'utf-8') or str(row.value), sheet.row(row_no)))

                        date_string = line[5]
                        date_string = date_string[:10]
                        try:
                            date_string = datetime.strptime(
                                date_string, '%d/%m/%Y').strftime('%Y-%m-%d')
                        except:
                            date_string = '01-01-01'
                            contador = contador + 1
                        if date_string != '01-01-01' and contador <= 100:
                            contador = 100
                            # if line[3] in (None, "", 0, '0'):
                            if line[3] <= line[4] or (line[3] in (None, "", 0, '0', "0")):
                                values.update({'date': date_string,
                                               'ref': line[0].decode("utf-8"),
                                               'partner': line[6],
                                               'memo': line[1].decode("utf-8"),
                                               'amount': int(line[4].replace('.', '')) / 10,
                                               })
                            else:
                                values.update({'date': date_string,
                                               'ref': line[0].decode("utf-8"),
                                               'partner': line[6],
                                               'memo': line[1].decode("utf-8"),
                                               # 'amount': line[3] * (-1),
                                               'amount': int(line[3].replace('.', '')) * (-1) / 10,
                                               })
                            res = self._create_statement_lines(values)
            elif self.bank_opt == 'itau':
                nombre_cartola = sheet.cell_value(1, 5)
                saldo_ini = sheet.cell_value(10, 3)
                saldo_fin = sheet.cell_value(10, 1)

                values = {'name': 'Cartola Itau',
                          'balance_start': saldo_ini,
                          'balance_end_real': saldo_fin

                          }

                active_id = (self._context.get('active_id'))
                bank_statement = self.env['account.bank.statement'].browse(
                    active_id)
                bank_statement.write(values)

                values_header = [sheet.cell_value(13, i) for i in range(sheet.ncols)]

                line = 14
                for row in range(14, sheet.nrows):
                    line += 1
                    vals = {}
                    values_es = {}

                    for col in range(sheet.ncols):
                        values_es[values_header[col]] = sheet.cell_value(row, col)

                    if values_es['Fecha'] == '':
                        break
                    if values_es['Movimientos'] == '':
                        break
                    if values_es['Movimientos'] == 'Saldo promedio últimos tres meses':
                        break

                    vals['statement_id'] = active_id

                    vals['date'] = datetime.now()

                    vals['payment_ref'] = values_es['Movimientos']
                    if values_es['Depósitos o abonos'] != '':
                        vals['amount'] = values_es['Depósitos o abonos']
                    if values_es['Giros o cargos'] != '':
                        vals['amount'] = - + values_es['Giros o cargos']

                    bank_statement.write({'line_ids': [(0, 0, vals)]})

    def _create_statement_lines(self, val):
        partner_id = self._find_partner(val.get('partner'))
        if not val.get('date'):
            raise Warning('Please Provide Date Field Value')
        if not val.get('memo'):
            raise Warning('Please Provide Memo Field Value')
        aaa = self._cr.execute(
            "insert into account_bank_statement_line (date,ref,partner_id,name,amount,statement_id) values (%s,%s,%s,%s,%s,%s)",
            (val.get(
                'date'), val.get('ref'), partner_id, val.get('memo'), val.get('amount'),
             self._context.get('active_id')))
        return True

    #

    def _find_partner(self, name):
        partner_id = self.env['res.partner'].search([('name', '=', name)])
        if partner_id:
            return partner_id.id
        else:
            return