# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class User(models.Model):
    _inherit = 'res.users'

    managements_ids = fields.One2many(  string='Gestiones SAC',
                                        comodel_name='sac.management',
                                        inverse_name='user_id')
