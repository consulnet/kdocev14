# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class RechazarReparacion(models.TransientModel):
    _name = 'kdoce.reparacion.rechazar'

    _description = "Asistente para rechazar reparaciones"

    def _default_reparacion(self):
        return self.env['kdoce.reparacion'].browse(self._context.get('active_id'))

    reparacion_id = fields.Many2one(comodel_name="kdoce.reparacion",
                                string="Reparacion",
                                required=True,
                                default=_default_reparacion)

    razon = fields.Text(string="Razón", required=True)

    def rechazar_reparacion(self):
        self.reparacion_id.detalle = "RECHAZADO: "+self.razon + "\n" + self.reparacion_id.detalle
        self.reparacion_id.estado = 'rechazado'
