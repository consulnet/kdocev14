# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class ResolverReparacion(models.TransientModel):
    _name = 'kdoce.reparacion.resolver'

    _description = "Asistente para resolver reparaciones"

    def _default_reparacion(self):
        return self.env['kdoce.reparacion'].browse(self._context.get('active_id'))

    reparacion_id = fields.Many2one(comodel_name="kdoce.reparacion",
                                string="Reparacion",
                                required=True,
                                default=_default_reparacion)

    solucion = fields.Text(string="Solución", required=True)

    def resolver_reparacion(self):
        self.reparacion_id.detalle = "Resuelto: "+self.solucion + "\n" + self.reparacion_id.detalle
        self.reparacion_id.estado = 'reparado'
        #self.reparacion_id.write({'estado': 'reparado', 'detalle': self.razon})
