# -*- coding: utf-8 -*-
{
    'name': 'Reparaciones',
    'summary': "Gestión de fallas en equipos y solicitud de presupuestos.",
    'description': """
        El objetivo de este módulo mantener un registro de los equipos con fallas de fábrica para poder hacer las solicitudes de reposición o repuestos al proveedor.
        Actual:
            - Registrar las fallas en los equipos comprados.
        Futuro:
            - Registrar las reparaciones realizadas por el personal de la empresa.

    """,
    'author': 'Matías Salomón',
    'website': 'https://kdoce.cl',
    'version': '0.3',
    'depends': ['mail','purchase','web'],
    'data': [
        'security/reparacion_security.xml',
        'security/ir.model.access.csv',
	    'views/reparacion_views.xml',
        'views/reparacion_menuitems.xml',
        'views/res_partner_views_inherit.xml',
        'views/res_user_views_inherit.xml',
        'wizards/rechazar_reparacion.xml',
        'wizards/resolver_reparacion.xml'
    ],
    'demo': [

    ]
}