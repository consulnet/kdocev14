# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class User(models.Model):
    _inherit = 'res.users'

    reparacion_ids = fields.One2many(  string='Reparaciones registradas',
                                        comodel_name='kdoce.reparacion',
                                        inverse_name='user_id')
