# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class Reparacion(models.Model):
    
    _name = "kdoce.reparacion"
    
    _description = "Reparación"

    _inherit = ['mail.thread']
    
    name = fields.Char(string="Título", required=True)

    detalle = fields.Text(string="Detalle", required=True)

    partner_id = fields.Many2one(string="Proveedor", comodel_name="res.partner")

    product_id = fields.Many2one(string="Producto", comodel_name="product.template", required=True)

    purchase_order_id = fields.Many2one(string="Orden de compra", comodel_name="purchase.order")

    product_detail = fields.Char(string="Detalle del producto",
                                related="product_id.name",
                                readonly=False,
                                required=True)


    user_id = fields.Many2one(  string="Responsable",
                                comodel_name="res.users",
                                default=lambda self: self.env.user and self.env.user.id or False)

    reparable = fields.Boolean(string="Reparable en Santiago", default=True)

    numeros_de_serie = fields.Text(string="Números de serie", required=True)

    repuestos = fields.Text(string="Repuestos necesarios")

    estado = fields.Selection(string="Estado",
                                 selection=[
                                            ('recibido', 'Recibido'),
                                            ('consulta', 'Consulta enviada'),
                                            ('enviado', 'Enviado al proveedor'),
                                            ('reparado', 'Reparado'),
                                            ('rechazado', 'Rechazado')
                                            ],
                                default="recibido"
                             )
    
    def button_rechazar(self):
        for rec in self:
            rec.write({'estado': 'rechazado'})

class data(models.Model):
    _name = 'kdoce.rechazo'
    name = fields.Char('Name')
    age= fields.Integer('Age')

    def btn_wizard(self):
        return {
            'name': 'Amount of times to be cloned',
            'type': 'ir.actions.act_window',
            'res_model': 'sf.datawizard',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            }

