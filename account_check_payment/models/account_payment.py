# -*- coding: utf-8 -*-

from odoo import fields, models, api


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    number_check = fields.Integer(string='Numero de cheque')
    check_name = fields.Char(string='Nombre del cheque')
    check_date_start = fields.Date(string='Fecha de emision del cheque')
    check_date_payment = fields.Date(string='Fecha de pago del cheque')
    check_cuit = fields.Char(string='CUIT del propietario del cheque')
    name_propietary_check = fields.Char(string='Nombre del propietario del cheque')
    code = fields.Char(string='Codigo')
    invoice_id = fields.Many2one('account.move', 'Factura')
    state_check = fields.Selection(string='Estado de Pago',
                                   selection=[('deposited', 'Depositado'),
                                              ('not_deposited', 'No Depositado')],
                                   default='not_deposited', store=True, compute='_compute_state_check_batch')

    @api.depends('batch_payment_id.state')
    def _compute_state_check_batch(self):
        for record in self:
            state = 'not_deposited'
            if record.batch_payment_id:
                if record.batch_payment_id.state == 'reconciled':
                    state = 'deposited'
            record.state_check = state


